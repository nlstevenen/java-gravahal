package com.springapp.mvc.gravaHal.game

import org.junit.Assert
import spock.lang.Specification

/**
 * Created by Steven Kok on 31/03/2015.
 */
class GravaHalTest extends Specification {

    private GravaHal gravaHal;

    def setup() {
        gravaHal = new GravaHal(new GameId(new PlayerId("PlayerOne"), new PlayerId("PlayerTwo")));
        gravaHal.activePlayer = Player.PLAYER_ONE
    }

    def "Game should be initialized with empty grava hals and n pits"() {
        expect:
        Assert.assertEquals(GravaHal.GRAVAHAL_INITIAL_VALUE, gravaHal.gravaHalls[Player.PLAYER_ONE.get()])
        Assert.assertEquals(GravaHal.GRAVAHAL_INITIAL_VALUE, gravaHal.gravaHalls[Player.PLAYER_TWO.get()])
        Assert.assertEquals(GravaHal.PIT_SIZE, gravaHal.pits[Player.PLAYER_ONE.get()].length)
        Assert.assertEquals(GravaHal.PIT_SIZE, gravaHal.pits[Player.PLAYER_TWO.get()].length)
    }

    def "Each pit should contain n items"() {
        expect:
        for (int[] pits : gravaHal.pits) {
            for (int pit : pits) {
                Assert.assertEquals(GravaHal.INITIAL_ITEMS_PER_PIT, pit)
            }
        }
    }

    def "If last item ends in grava hal player can go another turn"() {
        when:
        gravaHal.action(0)

        then:
        Assert.assertEquals(Player.PLAYER_ONE, gravaHal.activePlayer)
    }

    def "If last item lands on a pit, it is the other players turn"() {
        when:
        gravaHal.action(2)

        then:
        Assert.assertEquals(Player.PLAYER_TWO, gravaHal.activePlayer)
    }

    def "Activation of pit distributes items over following pits"() {
        when:
        gravaHal.action(0)

        then:
        Assert.assertEquals(0, gravaHal.pits[Player.PLAYER_ONE.get()][0])
        Assert.assertEquals(7, gravaHal.pits[Player.PLAYER_ONE.get()][1])
        Assert.assertEquals(7, gravaHal.pits[Player.PLAYER_ONE.get()][2])
        Assert.assertEquals(7, gravaHal.pits[Player.PLAYER_ONE.get()][3])
        Assert.assertEquals(7, gravaHal.pits[Player.PLAYER_ONE.get()][4])
        Assert.assertEquals(7, gravaHal.pits[Player.PLAYER_ONE.get()][5])
        Assert.assertEquals(1, gravaHal.gravaHalls[Player.PLAYER_ONE.get()])
    }

    def "Should throw exception with invalid pitId"() {
        when:
        gravaHal.action(pitId)

        then:
        thrown(IllegalArgumentException.class)

        where:
        pitId                 || _
        -1                    || _
        GravaHal.PIT_SIZE + 1 || _
        GravaHal.PIT_SIZE     || _
    }

    def "Activation of empty pit should do nothing"() {
        setup:
        gravaHal.pits[Player.PLAYER_ONE.get()][0] = 0;

        when:
        gravaHal.action(0)

        then:
        Assert.assertEquals(0, gravaHal.pits[Player.PLAYER_ONE.get()][0])
        Assert.assertEquals(6, gravaHal.pits[Player.PLAYER_ONE.get()][1])
        Assert.assertEquals(6, gravaHal.pits[Player.PLAYER_ONE.get()][2])
        Assert.assertEquals(6, gravaHal.pits[Player.PLAYER_ONE.get()][3])
        Assert.assertEquals(6, gravaHal.pits[Player.PLAYER_ONE.get()][4])
        Assert.assertEquals(6, gravaHal.pits[Player.PLAYER_ONE.get()][5])
        Assert.assertEquals(0, gravaHal.gravaHalls[Player.PLAYER_ONE.get()])
    }

    def "If last item lands on empty pit, take item and opponents items of same index and store them in gravahal"() {
        setup:
        gravaHal.pits[Player.PLAYER_ONE.get()] = inputPlayerOne
        gravaHal.pits[Player.PLAYER_TWO.get()] = inputPlayerTwo

        when:
        gravaHal.action(1)

        then:
        Assert.assertArrayEquals((int[]) expectedPlayerOne, gravaHal.pits[Player.PLAYER_ONE.get()])
        Assert.assertEquals(gravaHalPlayerOne, gravaHal.gravaHalls[Player.PLAYER_ONE.get()])

        Assert.assertArrayEquals((int[]) expectedPlayerTwo, gravaHal.pits[Player.PLAYER_TWO.get()])
        Assert.assertEquals(gravaHalPlayerTwo, gravaHal.gravaHalls[Player.PLAYER_TWO.get()])

        Assert.assertEquals(Player.PLAYER_TWO, gravaHal.activePlayer)

        where:
        inputPlayerOne     | inputPlayerTwo     | expectedPlayerOne  | expectedPlayerTwo  | gravaHalPlayerOne | gravaHalPlayerTwo
        [0, 6, 6, 6, 6, 6] | [9, 6, 6, 6, 6, 6] | [0, 0, 7, 7, 7, 7] | [0, 6, 6, 6, 6, 6] | 11                | 0
        [0, 7, 7, 7, 7, 7] | [6, 6, 6, 6, 6, 6] | [1, 0, 8, 8, 8, 8] | [6, 0, 6, 6, 6, 6] | 8                 | 0

    }

    def "Should be ended when either player's pit is empty"() {
        setup:
        gravaHal.pits[Player.PLAYER_ONE.get()] = pitPlayerOne;
        gravaHal.pits[Player.PLAYER_TWO.get()] = pitPlayerTwo;

        when:
        gravaHal.action(5)

        then:
        Assert.assertEquals(errorMessage, true, gravaHal.isGameFinished())

        where:
        pitPlayerOne       | pitPlayerTwo       | errorMessage
        [0, 0, 0, 0, 0, 1] | [6, 6, 6, 6, 6, 6] | "first one element"
        [0, 0, 0, 0, 0, 0] | [0, 0, 0, 0, 0, 1] | "first already empty"
        [0, 0, 0, 0, 0, 0] | [0, 0, 0, 0, 0, 0] | "both already empty"
    }

    def "When landing on empty pit, should transfer opponent's items"() {
        setup:
        gravaHal.pits[Player.PLAYER_ONE.get()] = [0, 6, 6, 6, 6, 6]
        gravaHal.pits[Player.PLAYER_TWO.get()] = [6, 6, 6, 6, 6, 6]

        when:
        gravaHal.action(1)

        then:
        Assert.assertEquals(8, gravaHal.gravaHalls[Player.PLAYER_ONE.get()])
    }

    def "Should return index of last item placed"() {
        when:
        def lastItemPlaced = gravaHal.distributeItems(0, itemsToPlace)

        then:
        Assert.assertEquals(expectedIndexLastItemPlaced, lastItemPlaced)

        where:
        itemsToPlace | expectedIndexLastItemPlaced
        1            | 0
        2            | 1
        3            | 2
        4            | 3
        5            | 4
        6            | 5
        7            | -1
        8            | 0
        9            | 1
    }

    def "Should empty pits and add points to gravaHal"() {
        when:
        gravaHal.emptyPitsInGravaHal()

        then:
        Assert.assertEquals(36, gravaHal.gravaHalls[Player.PLAYER_ONE.get()])
        Assert.assertEquals(36, gravaHal.gravaHalls[Player.PLAYER_TWO.get()])

        for (int[] pits : gravaHal.pits) {
            for (int pit : pits) {
                Assert.assertEquals(0, pit)
            }
        }
    }
}
