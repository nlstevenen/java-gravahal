package com.springapp.mvc.gravaHal.controllers;

import com.springapp.mvc.gravaHal.exceptions.GameNotFoundException;
import com.springapp.mvc.gravaHal.game.GameId;
import com.springapp.mvc.gravaHal.game.GravaHal;
import com.springapp.mvc.gravaHal.game.Player;
import com.springapp.mvc.gravaHal.game.PlayerId;
import com.springapp.mvc.gravaHal.persistence.Persistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class GameController {
    private final Persistence persistence;

    @Autowired
    public GameController(Persistence persistence) {
        this.persistence = persistence;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String printWelcome(@RequestParam(value = "pitIndex", required = false) String pitIndex,
                               @RequestParam(value = "player", required = false) String player,
                               ModelMap model) {
        PlayerId playerOne = new PlayerId("playerOne");
        PlayerId playerTwo = new PlayerId("playerTwo");
        GravaHal game = getGame(playerOne, playerTwo);

        if (pitIndex != null || player != null) {
            if (isPlayersTurn(game, player)) {
                game.action(Integer.valueOf(pitIndex));
                persistence.updateGame(game.getGameId(), game);
            }
        }
        fillFields(model, playerOne, playerTwo, game);
        return "game";
    }

    private GravaHal getGame(PlayerId playerOne, PlayerId playerTwo) {
        GameId gameId = new GameId(playerOne, playerTwo);
        try {
            return persistence.getGame(gameId);
        } catch (GameNotFoundException e) {
            return new GravaHal(gameId);
        }
    }

    private void fillFields(ModelMap model, PlayerId playerOne, PlayerId playerTwo, GravaHal game) {
        int[] gravaHalls = game.getGravaHalls();
        int indexPlayerOne = Player.PLAYER_ONE.get();
        int indexPlayerTwo = Player.PLAYER_TWO.get();

        setPlayerNames(model, playerOne, playerTwo);
        setGravaHals(model, gravaHalls[indexPlayerOne], gravaHalls[indexPlayerTwo]);
        setPits(model, game, indexPlayerOne, indexPlayerTwo);
        setGameStatus(model, game);
    }

    private void setPlayerNames(ModelMap model, PlayerId playerOne, PlayerId playerTwo) {
        model.addAttribute("playerOneName", playerOne.getPlayerName());
        model.addAttribute("playerTwoName", playerTwo.getPlayerName());
    }

    private void setGravaHals(ModelMap model, int gravaHall, int gravaHall1) {
        model.addAttribute("playerOneGravaHal", gravaHall);
        model.addAttribute("playerTwoGravaHal", gravaHall1);
    }

    private void setPits(ModelMap model, GravaHal game, int indexPlayerOne, int indexPlayerTwo) {
        int[][] pits = game.getPits();
        for (int i = 0; i < pits[0].length; i++) {
            model.addAttribute("playerOnePit" + (i + 1), pits[indexPlayerOne][i]);
            model.addAttribute("playerTwoPit" + (i + 1), pits[indexPlayerTwo][i]);
        }
    }

    private void setGameStatus(ModelMap model, GravaHal game) {
        if (!game.isGameFinished()) {
            model.addAttribute("gameStatus", game.getActivePlayersId());
        } else {
            model.addAttribute("gameStatus", "Game over, winner: " + getPlayerWithHighestScore(game));
        }
    }

    public boolean isPlayersTurn(GravaHal game, String player) {
        return game.getActivePlayersId().getPlayerName().equals(player);
    }

    public PlayerId getPlayerWithHighestScore(GravaHal game) {
        int[] gravaHalls = game.getGravaHalls();
        int scorePlayerOne = gravaHalls[Player.PLAYER_ONE.get()];
        int scorePlayerTwo = gravaHalls[Player.PLAYER_TWO.get()];
        PlayerId playerOne = game.getGameId().getPlayerOne();
        PlayerId playerTwo = game.getGameId().getPlayerTwo();
        return scorePlayerOne > scorePlayerTwo ? playerOne : playerTwo;
    }
}