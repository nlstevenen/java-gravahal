package com.springapp.mvc.gravaHal.game;

/**
 * Created by Steven Kok on 31/03/2015.
 */
public enum Player {

    PLAYER_ONE(0),
    PLAYER_TWO(1);

    private final int id;

    Player(int id) {
        this.id = id;
    }

    public int get() {
        return id;
    }

    public Player switchPlayer() {
        if (id == PLAYER_ONE.get()) {
            return PLAYER_TWO;
        } else {
            return PLAYER_ONE;
        }
    }


}
