package com.springapp.mvc.gravaHal.game;

/**
 * Created by Steven Kok on 30/03/2015.
 */
public class PlayerId {
    private final String playerName;

    public PlayerId(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlayerId playerId = (PlayerId) o;

        return playerName.equals(playerId.playerName);

    }

    @Override
    public int hashCode() {
        return playerName.hashCode();
    }

    @Override
    public String toString() {
        return playerName;
    }
}
