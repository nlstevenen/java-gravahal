package com.springapp.mvc.gravaHal.game;

/**
 * Created by Steven Kok on 30/03/2015.
 */
public class GameId {
    private final PlayerId playerOne;
    private final PlayerId playerTwo;

    public GameId(PlayerId playerOne, PlayerId playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }

    public PlayerId getPlayerOne() {
        return playerOne;
    }

    public PlayerId getPlayerTwo() {
        return playerTwo;
    }

    @Override
    public String toString() {
        return "GameId{" +
                "playerOne=" + playerOne +
                ", playerTwo=" + playerTwo +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameId gameId = (GameId) o;

        if (!playerOne.equals(gameId.playerOne)) return false;
        if (!playerTwo.equals(gameId.playerTwo)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = playerOne.hashCode();
        result = 31 * result + playerTwo.hashCode();
        return result;
    }
}
