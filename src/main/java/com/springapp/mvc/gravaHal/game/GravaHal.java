package com.springapp.mvc.gravaHal.game;

/**
 * Created by Steven Kok on 30/03/2015.
 */
public class GravaHal {

    public static final int PIT_SIZE = 6;
    public static final int GRAVAHAL_INITIAL_VALUE = 0;
    public static final int INITIAL_ITEMS_PER_PIT = 6;
    private final GameId gameId;
    private int[] gravaHalls;
    private int[][] pits;
    private Player activePlayer;

    public GravaHal(GameId gameId) {
        activePlayer = Player.PLAYER_ONE;
        this.gameId = gameId;
        initializePits();
        initializeGravalHalls();
    }

    public PlayerId getActivePlayersId() {
        if (activePlayer.equals(Player.PLAYER_ONE)) {
            return getGameId().getPlayerOne();
        } else {
            return getGameId().getPlayerTwo();
        }
    }

    public int[] getGravaHalls() {
        return gravaHalls;
    }

    private void initializeGravalHalls() {
        gravaHalls = new int[2];
        gravaHalls[Player.PLAYER_ONE.get()] = GRAVAHAL_INITIAL_VALUE;
        gravaHalls[Player.PLAYER_TWO.get()] = GRAVAHAL_INITIAL_VALUE;
    }

    private void initializePits() {
        pits = new int[2][PIT_SIZE];
        for (int i = 0; i < PIT_SIZE; i++) {
            pits[Player.PLAYER_ONE.get()][i] = INITIAL_ITEMS_PER_PIT;
            pits[Player.PLAYER_TWO.get()][i] = INITIAL_ITEMS_PER_PIT;
        }
    }

    public GameId getGameId() {
        return gameId;
    }

    /**
     * startingPit; first pit == 0
     */
    public void action(int pitId) {
        if (isGameFinished()) return;
        confirmPitId(pitId);
        int items = removeItemsFromPit(pitId, activePlayer);
        if (noMoreItems(items)) return;

        int indexLastItemPlaced = distributeItems(pitId + 1, items);

        if (!lastItemWasPlacedInGravaHal(indexLastItemPlaced)
                && lastItemWasPlacedInEmptyPit(indexLastItemPlaced)) {
            transferOpponentsItemsToGravaHal(indexLastItemPlaced);
            transferLastItemPlacedToGravaHal(indexLastItemPlaced);
        }

        if (isGameFinished()) {
            emptyPitsInGravaHal();
        }

        if (!lastItemWasPlacedInGravaHal(indexLastItemPlaced)) {
            activePlayer = activePlayer.switchPlayer();
        }
    }

    private void transferLastItemPlacedToGravaHal(int indexLastItemPlaced) {
        gravaHalls[activePlayer.get()] += 1;
        pits[activePlayer.get()][indexLastItemPlaced] = 0;
    }

    private boolean lastItemWasPlacedInEmptyPit(int indexLastItemPlaced) {
        return pits[activePlayer.get()][indexLastItemPlaced] == 1;
    }

    private boolean lastItemWasPlacedInGravaHal(int indexLastItemPlaced) {
        return indexLastItemPlaced == -1;
    }

    public int[][] getPits() {
        return pits;
    }

    private void emptyPitsInGravaHal() {
        int itemsFromPitsPlayerOne = 0;
        int itemsFromPitsPlayerTwo = 0;
        for (int i = 0; i < PIT_SIZE; i++) {
            itemsFromPitsPlayerOne += pits[Player.PLAYER_ONE.get()][i];
            pits[Player.PLAYER_ONE.get()][i] = 0;
            itemsFromPitsPlayerTwo += pits[Player.PLAYER_TWO.get()][i];
            pits[Player.PLAYER_TWO.get()][i] = 0;
        }
        gravaHalls[Player.PLAYER_ONE.get()] += itemsFromPitsPlayerOne;
        gravaHalls[Player.PLAYER_TWO.get()] += itemsFromPitsPlayerTwo;
    }

    public boolean isGameFinished() {
        boolean pitOneEmpty = true;
        boolean pitTwoEmpty = true;

        for (int i = 0; i < PIT_SIZE; i++) {
            int pitPlayerOne = pits[Player.PLAYER_ONE.get()][i];
            int pitPlayerTwo = pits[Player.PLAYER_TWO.get()][i];
            if (pitPlayerOne > 0) pitOneEmpty = false;
            if (pitPlayerTwo > 0) pitTwoEmpty = false;
        }
        return pitOneEmpty || pitTwoEmpty;
    }

    /*
        Returns index of last placed item, -1 if it was in GravaHal
     */
    private int distributeItems(int startingPit, int items) {
        for (int i = startingPit; i < PIT_SIZE; i++) {
            pits[activePlayer.get()][i] += 1;
            items--;
            if (noMoreItems(items)) {
                return i;
            }
        }
        assert items > 0;
        gravaHalls[activePlayer.get()] += 1;
        items--;
        if (noMoreItems(items)) {
            return -1; //last item placed in GravaHal
        } else {
            return distributeItems(0, items);
        }
    }

    private boolean noMoreItems(int items) {
        return items == 0;
    }

    private void transferOpponentsItemsToGravaHal(int pitIndex) {
        int opponentsItems = removeItemsFromPit(pitIndex, activePlayer.switchPlayer());
        gravaHalls[activePlayer.get()] += opponentsItems;
    }

    private int removeItemsFromPit(int pitId, Player playersTurn) {
        int items = pits[playersTurn.get()][pitId];
        pits[playersTurn.get()][pitId] = 0;
        return items;
    }

    private void confirmPitId(int pitId) {
        if (pitId < 0) {
            throw new IllegalArgumentException("PitId to small: " + pitId);
        } else if (pitId >= PIT_SIZE) {
            throw new IllegalArgumentException("PitId to big: " + pitId);
        }
    }
}
