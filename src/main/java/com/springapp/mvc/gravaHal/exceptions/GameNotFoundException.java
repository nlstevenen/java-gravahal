package com.springapp.mvc.gravaHal.exceptions;

/**
 * Created by Steven Kok on 30/03/2015.
 */
public class GameNotFoundException extends Exception {
    public GameNotFoundException(String message) {
        super(message);
    }
}
