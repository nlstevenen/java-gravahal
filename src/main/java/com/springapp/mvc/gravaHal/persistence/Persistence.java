package com.springapp.mvc.gravaHal.persistence;

import com.springapp.mvc.gravaHal.exceptions.GameNotFoundException;
import com.springapp.mvc.gravaHal.game.GameId;
import com.springapp.mvc.gravaHal.game.GravaHal;

/**
 * Created by Steven Kok on 30/03/2015.
 */
public interface Persistence {
    GravaHal getGame(GameId gameId) throws GameNotFoundException;

    void updateGame(GameId gameId, GravaHal updatedGame);
}
