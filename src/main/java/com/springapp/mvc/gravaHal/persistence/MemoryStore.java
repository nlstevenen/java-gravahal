package com.springapp.mvc.gravaHal.persistence;

import com.springapp.mvc.gravaHal.exceptions.GameNotFoundException;
import com.springapp.mvc.gravaHal.game.GameId;
import com.springapp.mvc.gravaHal.game.GravaHal;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Steven Kok on 30/03/2015.
 */
@Component
public class MemoryStore implements Persistence {
    private final Map<GameId, GravaHal> activeGames;

    public MemoryStore() {
        this.activeGames = new HashMap<GameId, GravaHal>();
    }

    @Override
    public GravaHal getGame(GameId gameId) throws GameNotFoundException {
        GravaHal gravaHal = activeGames.get(gameId);
        if (gravaHal == null) {
            throw new GameNotFoundException("No game found for ID: " + gameId);
        }
        return gravaHal;
    }

    @Override
    public void updateGame(GameId gameId, GravaHal updatedGame) {
        this.activeGames.put(gameId, updatedGame);
    }
}
