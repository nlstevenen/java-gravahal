/**
 * Created by Steven Kok on 30/03/2015.
 */

function clickedPit(clickedDiv) {
    var pitIndex = clickedDiv.getAttribute("pitIndex");
    var player = clickedDiv.getAttribute("player");
    window.location = "/?pitIndex=" + pitIndex + "&player=" + player;
}