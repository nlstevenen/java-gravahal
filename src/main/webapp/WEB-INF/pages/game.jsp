<html>
<head>
    <link href="${pageContext.request.contextPath}resources/css/game.css" rel="stylesheet">
    <script src="${pageContext.request.contextPath}resources/javascript/game.js"></script>
</head>
<body>
<div id="outerDiv">
    <div id="outerLeft"></div>
    <div id="outerCenter">
        <div id="gameBoard">
            <div class="gravaHal">
                ${playerOneName}

                ${playerOneGravaHal}
            </div>
            <div id="pits">
                <div id="pitsPlayerOne">
                    <div pitIndex="0" class="pitPlayerOne" player="playerOne"
                         onclick="clickedPit(this)">${playerOnePit1}</div>
                    <div pitIndex="1" class="pitPlayerOne" player="playerOne"
                         onclick="clickedPit(this)">${playerOnePit2}</div>
                    <div pitIndex="2" class="pitPlayerOne" player="playerOne"
                         onclick="clickedPit(this)">${playerOnePit3}</div>
                    <div pitIndex="3" class="pitPlayerOne" player="playerOne"
                         onclick="clickedPit(this)">${playerOnePit4}</div>
                    <div pitIndex="4" class="pitPlayerOne" player="playerOne"
                         onclick="clickedPit(this)">${playerOnePit5}</div>
                    <div pitIndex="5" class="pitPlayerOne" player="playerOne"
                         onclick="clickedPit(this)">${playerOnePit6}</div>
                </div>
                <div id="pitsPlayerTwo">
                    <div pitIndex="0" class="pitPlayerTwo" player="playerTwo"
                         onclick="clickedPit(this)">${playerTwoPit1}</div>
                    <div pitIndex="1" class="pitPlayerTwo" player="playerTwo"
                         onclick="clickedPit(this)">${playerTwoPit2}</div>
                    <div pitIndex="2" class="pitPlayerTwo" player="playerTwo"
                         onclick="clickedPit(this)">${playerTwoPit3}</div>
                    <div pitIndex="3" class="pitPlayerTwo" player="playerTwo"
                         onclick="clickedPit(this)">${playerTwoPit4}</div>
                    <div pitIndex="4" class="pitPlayerTwo" player="playerTwo"
                         onclick="clickedPit(this)">${playerTwoPit5}</div>
                    <div pitIndex="5" class="pitPlayerTwo" player="playerTwo"
                         onclick="clickedPit(this)">${playerTwoPit6}</div>
                </div>
            </div>
            <div class="gravaHal">
                ${playerTwoName}
                ${playerTwoGravaHal}
            </div>
        </div>
    </div>
    <div id="outerRight">${gameStatus}</div>
</div>
</body>
</html>

